package com.test.bluetooth.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.bluetooth.model.DeviceModel
import com.test.bluetooth.R
import com.test.bluetooth.databinding.DeviceItemBinding

class DeviceAdapter : RecyclerView.Adapter<DeviceAdapter.ViewHolder>() {

    var items: List<DeviceModel> = mutableListOf()

    inner class ViewHolder(private val binding: DeviceItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(deviceModel: DeviceModel) {
            with(binding) {
                deviceId.text = deviceModel.id
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<DeviceItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.device_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun submitList(list: List<DeviceModel>) {
        items = list
        notifyDataSetChanged()
    }
}
