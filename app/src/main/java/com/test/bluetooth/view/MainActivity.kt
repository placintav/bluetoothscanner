package com.test.bluetooth.view

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.test.bluetooth.model.DeviceModel
import com.test.bluetooth.vm.MainViewModel
import com.test.bluetooth.R
import com.test.bluetooth.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    companion object {
        const val ONE_SECOND = 1000L
        const val FINE_LOCATION_PERMISSION = 1234
    }

    val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }
    private lateinit var binding: ActivityMainBinding
    lateinit var receiver: BroadcastReceiver
    lateinit var timer: Timer
    var adapter: BluetoothAdapter? = null
    val list = mutableListOf<DeviceModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_main
        )
        binding.model = viewModel
        adapter = BluetoothAdapter.getDefaultAdapter()
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                intent?.let { broadcastIntent ->
                    broadcastIntent.action?.let { action ->
                        when (action) {
                            BluetoothDevice.ACTION_FOUND -> {
                                val foundDevice: BluetoothDevice? =
                                    intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                                foundDevice?.let {
                                    list.add(
                                        DeviceModel(
                                            it.address
                                        )
                                    )
                                }
                            }
                            else -> {
                            }
                        }
                    }
                }
            }

        }
        registerReceiver(receiver, filter)
    }

    override fun onResume() {
        super.onResume()
        if (checkPermissions()
        ) {
            startScanning()
        } else {
            requestPermissions()
        }
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            FINE_LOCATION_PERMISSION
        )
    }

    private fun checkPermissions(): Boolean {
        return (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun startScanning() {
        timer = Timer()
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    viewModel.adapter.submitList(list.toList())
                    list.clear()
                    adapter?.cancelDiscovery()
                    adapter?.startDiscovery()
                }
            }

        },
            ONE_SECOND,
            ONE_SECOND
        )
    }

    override fun onPause() {
        super.onPause()
        stopScanning()
    }

    private fun stopScanning() {
        if (::timer.isInitialized) {
            timer.cancel()
            timer.purge()
        }
        adapter?.cancelDiscovery()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiver)
    }
}
