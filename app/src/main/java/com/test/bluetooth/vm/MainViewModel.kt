package com.test.bluetooth.vm

import androidx.lifecycle.ViewModel
import com.test.bluetooth.adapter.DeviceAdapter

class MainViewModel : ViewModel() {

    val adapter: DeviceAdapter by lazy {
        DeviceAdapter()
    }

}