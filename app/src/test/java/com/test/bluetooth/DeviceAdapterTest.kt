package com.test.bluetooth

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBinderMapperImpl
import androidx.databinding.DataBindingUtil
import com.test.bluetooth.adapter.DeviceAdapter
import com.test.bluetooth.databinding.DeviceItemBinding
import com.test.bluetooth.model.DeviceModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.powermock.api.mockito.PowerMockito
import org.powermock.api.support.membermodification.MemberMatcher
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.reflect.Whitebox

@RunWith(PowerMockRunner::class)
@PrepareForTest(
    DeviceAdapter::class, DeviceAdapter.ViewHolder::class, LayoutInflater::class,
    DataBindingUtil::class
)
class DeviceAdapterTest {

    companion object {
        const val DEVICE_ID = "01:23:45:67"
    }

    private lateinit var testAdapter: DeviceAdapter

    private val mockBinding = Mockito.mock(DeviceItemBinding::class.java)
    private val mockViewHolder = Mockito.mock(DeviceAdapter.ViewHolder::class.java)
    private lateinit var testViewHolder: DeviceAdapter.ViewHolder
    private val mockViewGroup = Mockito.mock(ViewGroup::class.java)
    private val mockView = Mockito.mock(View::class.java)
    private val mockTextView = Mockito.mock(TextView::class.java)
    private var mockLayoutInflater: LayoutInflater =
        Mockito.mock(LayoutInflater::class.java, Mockito.withSettings().stubOnly())
    private var mockContext: Context = Mockito.mock(Context::class.java)
    private lateinit var deviceOne: DeviceModel

    @Before
    fun setup() {
        testAdapter = Mockito.spy(DeviceAdapter())
        val mockMapper = Mockito.mock(DataBinderMapperImpl::class.java)
        PowerMockito.whenNew(DataBinderMapperImpl::class.java).withAnyArguments()
            .thenReturn(mockMapper)
        PowerMockito.mockStatic(LayoutInflater::class.java, DataBindingUtil::class.java)
        PowerMockito.`when`(LayoutInflater.from(mockContext)).thenReturn(mockLayoutInflater)
        PowerMockito.`when`(mockViewGroup.context).thenReturn(mockContext)
        PowerMockito.`when`(mockBinding.root).thenReturn(mockView)
        PowerMockito.`when`(
                DataBindingUtil.inflate(
                    mockLayoutInflater,
                    R.layout.device_item,
                    mockViewGroup,
                    false
                ) as? DeviceItemBinding
            )
            .thenReturn(mockBinding)
        deviceOne = DeviceModel(DEVICE_ID)
        testAdapter.items = listOf(deviceOne)
        Whitebox.setInternalState(mockBinding, "deviceId", mockTextView)
    }

    @Test
    fun onCreateViewHolder() {
        Assert.assertNotNull(testAdapter.onCreateViewHolder(mockViewGroup, 0))
    }

    @Test
    fun getItemCount() {
        Assert.assertEquals(1, testAdapter.itemCount)
    }

    @Test
    fun onBindViewHolder() {
        PowerMockito.suppress(MemberMatcher.methodsDeclaredIn(DeviceAdapter.ViewHolder::class.java))
        testAdapter.onBindViewHolder(mockViewHolder, 0)
        Mockito.verify(mockViewHolder).bind(deviceOne)
    }

    @Test
    fun bind() {
        testViewHolder = Mockito.spy(testAdapter.ViewHolder(mockBinding))
        testViewHolder.bind(deviceOne)
        Mockito.verify(mockTextView).text = DEVICE_ID
    }
}